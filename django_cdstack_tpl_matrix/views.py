import copy

import yaml
from django.template import engines
from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_whclient.django_cdstack_tpl_whclient.views import (
    handle as handle_whclient,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_matrix/django_cdstack_tpl_matrix"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_packages_purge" not in template_opts:
        template_opts["apt_packages_purge"] = list()

    if "matrix_turn_uri_list" in template_opts:
        template_opts["matrix_turn_uris"] = template_opts["matrix_turn_uri_list"].split(
            ","
        )

    # appservice-registration
    with open(
        module_prefix
        + "/templates/config-fs/dynamic/etc/matrix-synapse/appservice-registration.yaml",
        "r",
    ) as stream:
        matrix_appservice_registration_template = yaml.safe_load(stream)

    # matrix-appservice-discord
    with open(
        module_prefix
        + "/templates/image/copy-to-filesystem/opt/matrix-appservices/matrix-appservice-discord/config/config.sample.yaml",
        "r",
    ) as stream:
        matrix_appservice_discord_config = yaml.safe_load(stream)

    zip_add_file(
        zipfile_handler,
        "opt/matrix-appservices/matrix-appservice-discord/config/config.yaml",
        yaml.dump(matrix_appservice_discord_config),
    )

    # matrix-appservice-irc
    with open(
        module_prefix
        + "/templates/image/copy-to-filesystem/opt/matrix-appservices/matrix-appservice-irc/config.sample.yaml",
        "r",
    ) as stream:
        matrix_appservice_irc_config = yaml.safe_load(stream)

    matrix_appservice_irc_config["homeserver"]["url"] = "https://{}".format(
        template_opts["node_hostname_fqdn"]
    )
    matrix_appservice_irc_config["homeserver"]["domain"] = template_opts[
        "matrix_domain"
    ]

    matrix_appservice_irc_config["database"]["connectionString"] = (
        "postgres://{}:{}@{}:{}/{}".format(
            template_opts["matrix_appsrv_irc_database_user"],
            template_opts["matrix_appsrv_irc_database_password"],
            template_opts["matrix_appsrv_irc_database_host"],
            template_opts["matrix_appsrv_irc_database_port"],
            template_opts["matrix_appsrv_irc_database_name"],
        )
    )

    matrix_appservice_irc_config["ircService"]["servers"] = dict()

    matrix_appservice_irc_config["ircService"]["servers"]["irc.libera.chat"] = dict()

    matrix_appservice_irc_config["ircService"]["servers"]["irc.libera.chat"][
        "additionalAddresses"
    ] = list()
    matrix_appservice_irc_config["ircService"]["servers"]["irc.libera.chat"][
        "additionalAddresses"
    ].append("irc.libera.chat")
    matrix_appservice_irc_config["ircService"]["servers"]["irc.libera.chat"][
        "port"
    ] = 6697
    matrix_appservice_irc_config["ircService"]["servers"]["irc.libera.chat"][
        "ssl"
    ] = True

    zip_add_file(
        zipfile_handler,
        "opt/matrix-appservices/matrix-appservice-irc/config.yaml",
        yaml.dump(matrix_appservice_irc_config),
    )

    matrix_appservice_irc_registration = copy.deepcopy(
        matrix_appservice_registration_template
    )

    matrix_appservice_irc_registration["id"] = "matrix-appservice-irc"
    matrix_appservice_irc_registration["hs_token"] = template_opts[
        "matrix_appservice_irc_registration_hs_token"
    ]
    matrix_appservice_irc_registration["as_token"] = template_opts[
        "matrix_appservice_irc_registration_as_token"
    ]
    matrix_appservice_irc_registration["url"] = "http://localhost:9999"
    matrix_appservice_irc_registration["sender_localpart"] = "irc_bot"
    matrix_appservice_irc_registration["de.sorunome.msc2409.push_ephemeral"] = True
    matrix_appservice_irc_registration["protocols"] = ["irc"]
    matrix_appservice_irc_registration["namespaces"]["users"] = list()
    matrix_appservice_irc_registration["namespaces"]["users"].append(
        {
            "exclusive": True,
            "regex": "@irc\.libera\.chat_.*:team\.srvfarm\.net",
        }
    )

    zip_add_file(
        zipfile_handler,
        "opt/matrix-appservices/matrix-appservice-irc/appservice-registration-irc.yaml",
        yaml.dump(matrix_appservice_irc_registration),
    )

    # matrix-appservice-slack
    with open(
        module_prefix
        + "/templates/image/copy-to-filesystem/opt/matrix-appservices/matrix-appservice-slack/config/config.sample.yaml",
        "r",
    ) as stream:
        matrix_appservice_slack_config = yaml.safe_load(stream)

    zip_add_file(
        zipfile_handler,
        "opt/matrix-appservices/matrix-appservice-slack/config/config.yaml",
        yaml.dump(matrix_appservice_slack_config),
    )

    # matrix-bifrost
    with open(
        module_prefix
        + "/templates/image/copy-to-filesystem/opt/matrix-appservices/matrix-bifrost/config.sample.yaml",
        "r",
    ) as stream:
        matrix_bifrost_config = yaml.safe_load(stream)

    zip_add_file(
        zipfile_handler,
        "opt/matrix-appservices/matrix-bifrost/config.yaml",
        yaml.dump(matrix_bifrost_config),
    )

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_whclient(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    if not skip_handle_os:
        handle_deb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
